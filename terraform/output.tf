output "floating-ip-value-bastion" {
    value       = openstack_networking_floatingip_v2.floating-ip.address
    description = "Flaoting IP address of the bastion compute instance"
}

output "floating-ip-value-compute" {
    value       = openstack_networking_floatingip_v2.floating-ip-compute.address
    description = "Flaoting IP address of the bastion compute instance"
}