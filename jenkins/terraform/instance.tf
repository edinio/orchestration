data "template_file" "common-scripts" {
    template = file("./scripts/common.yaml")
}

data "template_file" "config-scripts" {
    template = file("./scripts/cloud-init.yaml")
    vars = {
        REGISTRATION_TOKEN = var.gitlab-runner-token
        RUNNER_NAME        = var.gitlab-runner-name
        RUNNER_DESCRIPTION = var.gitlab-runner-description
        RUNNER_EXECUTOR    = var.gitlab-runner-executor
    }
}

#data "template_file" "proxy-config" {
#    template = file("./scripts/proxy-config.sh")
#}

data "template_file" "ansible-config" {
    template = file("./scripts/ansible-config.sh")
    vars = {
        SSH_KEY = var.remote-path-to-ssh-key
    }
}

#data "template_file" "gitlab-runner-config" {
#    template = file("./scripts/gitlab-runner-config.sh")
#    vars = {
#        REGISTRATION_TOKEN = var.gitlab-runner-token
#        RUNNER_NAME        = var.gitlab-runner-name
#        RUNNER_DESCRIPTION = var.gitlab-runner-description
#        RUNNER_EXECUTOR    = var.gitlab-runner-executor
#    }
#}

resource "openstack_compute_instance_v2" "compute-instance" {
    name             = "server"
    image_name       = "imta-ubuntu"
    flavor_name      = var.instance_flavor
    key_pair         = var.ssh_key
    security_groups  = ["default", "${openstack_networking_secgroup_v2.server-secgroup.name}"]
    user_data        = data.template_file.common-scripts.rendered
    network {
        name        = openstack_networking_network_v2.private-network.name
        fixed_ip_v4 = "192.168.1.100"
    }
}

resource "openstack_compute_instance_v2" "bastion-instance" {
    name             = "bastion"
    image_name       = "imta-docker"
    flavor_name      = var.instance_flavor
    key_pair         = var.ssh_key
    security_groups  = ["default", "${openstack_networking_secgroup_v2.bastion-secgroup.name}"]
    user_data        = data.template_file.config-scripts.rendered
    network          {
        name = openstack_networking_network_v2.private-network.name
        fixed_ip_v4 = "192.168.1.50"
    }
}

# Attach Floating IP
resource "openstack_compute_floatingip_associate_v2" "srv-floating-ip" {
    floating_ip = openstack_networking_floatingip_v2.floating-ip.address
    instance_id = openstack_compute_instance_v2.bastion-instance.id
}

resource "openstack_compute_floatingip_associate_v2" "srv-floating-ip-compute" {
    floating_ip = openstack_networking_floatingip_v2.floating-ip-compute.address
    instance_id = openstack_compute_instance_v2.compute-instance.id
}

resource "null_resource" "copy-ssh-key" {
    depends_on = [
        openstack_compute_floatingip_associate_v2.srv-floating-ip
        ]
    connection {
        type        = "ssh"
        user        = var.username
        host        = openstack_networking_floatingip_v2.floating-ip.address
        private_key = "${file(var.local-path-to-ssh-key)}"
    }
    provisioner "file" {
        source      = var.local-path-to-ssh-key
        destination = var.remote-path-to-ssh-key
    }
    provisioner "remote-exec" {
        inline = [
            "chmod 600 ${var.remote-path-to-ssh-key}"
        ]
    }
}


resource "null_resource" "config-microk8s" {
    depends_on = [openstack_compute_floatingip_associate_v2.srv-floating-ip]
    connection {
        type        = "ssh"
        user        = var.username
        host        = openstack_networking_floatingip_v2.floating-ip.address
        private_key = "${file(var.local-path-to-ssh-key)}"
        #timeout     = "1m"
    }
    provisioner "remote-exec" {
        inline = [
            "mkdir /home/ubuntu/ansible",
            "mkdir /home/ubuntu/kubernetes"
        ]
    }
    provisioner "file" {
        source      = "./ansible"
        destination = "/home/ubuntu"
    }
    provisioner "file" {
        source      = "./kubernetes"
        destination = "/home/ubuntu"
    }
    provisioner "remote-exec" {
        inline = [
            "${data.template_file.ansible-config.rendered}"
        ]
    }
}